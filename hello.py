import os
import requests
import json
from elasticsearch import Elasticsearch
from flask import jsonify
from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from flask_script import Manager
from flask_migrate import Migrate, MigrateCommand

ES_CLUSTER = 'http://localhost:9200/'
ES_INDEX = 'etob'
ES_TYPE = 'etob_data'
jsonstr= ''

app = Flask(__name__)
x=os.environ.get("DATABASE_URL")
print (x)
app.config['SQLALCHEMY_DATABASE_URI'] = x
SQLALCHEMY_TRACK_MODIFICATIONS = True
db = SQLAlchemy(app)

migrate = Migrate(app, db)
manager = Manager(app)
manager.add_command('db', MigrateCommand)

class Post(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    title = db.Column(db.String(80), unique=True)
    post_text = db.Column(db.Text)
    
    def __init__(self, title, post_text):
        self.title = title
        self.post_text = post_text

@app.route('/register')
def hello():
    user = Post('testpython1003',"""{
	"mappings": {
		"ICSRJSON": {
			"properties": {
				"documentName": {
					"type": "text",
					"fields": {
						"keyword": {
							"type": "keyword",
							"ignore_above": 256
						}
					}
				},
				"eventObj": {
					"properties": {
						"eventName": {
							"type": "text",
							"fields": {
								"keyword": {
									"type": "keyword",
									"ignore_above": 256
								}
							}
						},
						"eventOutcome": {
							"type": "text",
							"fields": {
								"keyword": {
									"type": "keyword",
									"ignore_above": 256
								} 
							}
						},
						"icsrId": {
							"type": "text",
							"fields": {
								"keyword": {
									"type": "keyword",
									"ignore_above": 256
								}
							}
						}
					}
				},
				"patientObj": {
					"properties": {
						"icsrId": {
							"type": "text",
							"fields": {
								"keyword": {
									"type": "keyword",
									"ignore_above": 256
								}
							}
						},
						"patientAddress": {
							"type": "text",
							"fields": {
								"keyword": {
									"type": "keyword",
									"ignore_above": 256
								}
							}
						},
						"patientName": {
							"type": "text",
							"fields": {
								"keyword": {
									"type": "keyword",
									"ignore_above": 256
								}
							}
						}
					}
				},
				"productLt": {
					"properties": {
						"formulation": {
							"type": "text",
							"fields": {
								"keyword": {
									"type": "keyword",
									"ignore_above": 256
								}
							}
						},
						"icsrId": {
							"type": "text",
							"fields": {
								"keyword": {
									"type": "keyword",
									"ignore_above": 256
								}
							}
						},
						"tradeName": {
							"type": "text",
							"fields": {
								"keyword": {
									"type": "keyword",
									"ignore_above": 256
								}
							}
						}
					}
				},
				"reporterObj": {
					"properties": {
						"icsrId": {
							"type": "text",
							"fields": {
								"keyword": {
									"type": "keyword",
									"ignore_above": 256
								}
							}
						},
						"reporterName": {
							"type": "text",
							"fields": {
								"keyword": {
									"type": "keyword",
									"ignore_above": 256
								}
							}
						},
						"reporterTitle": {
							"type": "text",
							"fields": {
								"keyword": {
									"type": "keyword",
									"ignore_above": 256
								}
							}
						}
					}
				}
			}
		}
	}
}""")
    db.session.add(user)
    db.session.commit()
    jsonstr= user.post_text
    print ('hello Priya')
    indata = Post.query.filter_by(title='testpython1003').first()
    jsonString = json.loads(jsonstr)
    return "jsonstr123"
   
@app.route('/registertest')
def helloapp(jsonstr):
    return jsonstr
    
    #return jsonify(request.json)
    

	
if __name__ == "__main__":
    manager.run()